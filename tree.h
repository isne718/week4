#pragma once

#include <queue>
#include <stack>
#include <iostream>
using namespace std;

#ifndef Binary_Search_Tree
#define Binary_Search_Tree

template<class T> class Tree;

template<class T>
class Node {
public:
	Node() { left = right = NULL; }
	Node(const T& el, Node* l = 0, Node* r = 0) {
		key = el; left = l; right = r;
	}
	T key;
	Node* left, * right;
};

template<class T>
class Tree {
public:
	Tree() { root = 0; }
	~Tree() { clear(); }
	void clear() { clear(root); root = 0; }
	bool isEmpty() { return root == 0; }
	void inorder() { inorder(root); }
	void insert(const T& el);
	void findAndDelete(const T& el);
	void deleteNode(Node<T>*& node);
	bool Search(const T& el);
	int depth() { return depth(root); }
	void balance(vector<T>number, int first, int last);


protected:
	Node<T>* root;
	int depth(Node<T>* p);
	void clear(Node<T>* p);
	void inorder(Node<T>* p);

};

template<class T>
void Tree<T>::clear(Node<T>* p)
{
	if (p != 0) {
		clear(p->left);
		clear(p->right);
		delete p;
	}
}

template<class T>
void Tree<T>::inorder(Node<T>* p) {
	//TO DO! This is for an inorder tree traversal!
	if (p != NULL) {
		inorder(p->left);
		cout << p->key << " ";
		inorder(p->right);
	}
}

template<class T>
void Tree<T>::insert(const T& el) {
	Node<T>* p = root, * prev = 0;
	while (p != 0) {
		prev = p;
		if (p->key < el)
			p = p->right;
		else
			p = p->left;
	}
	if (root == 0)
		root = new Node<T>(el);
	else if (prev->key < el)
		prev->right = new Node<T>(el);
	else
		prev->left = new Node<T>(el);
}

template<class T>
void Tree<T>::findAndDelete(const T& el) {
	Node<T>* node = root, * prev = 0;
	while (node != 0) {
		if (node->key == el)
			break;
		prev = node;
		if (node->key < el)
			node = node->right;
		else node = node->left;
	}
	if (node != 0 && node->key == el)
		if (node == root)
			deleteNode(root);
		else if (prev->left == node)
			deleteNode(prev->left);
		else deleteNode(prev->right);
}

template<class T>
bool Tree<T>::Search(const T& el) {
	Node<T>* node = root, * prev = 0;
	while (node != 0) { //travel until find null
		if (node->key == el)
			return true;
		prev = node;
		if (node->key < el)
			node = node->right;
		else node = node->left;
	}
	return false;
}


template<class T>
void Tree<T>::deleteNode(Node<T>*& node) {
	Node<T>* prev, * tmp = node;
	if (node->right == 0)
		node = node->left;
	else if (node->left == 0)
		node = node->right;
	else {
		tmp = node->left;
		prev = node;
		while (tmp->right != 0) {
			prev = tmp;
			tmp = tmp->right;
		}
		node->key = tmp->key;
		if (prev == node)
			prev->left = tmp->left;
		else prev->right = tmp->left;
	}
	delete tmp;
}

template<class T>
int Tree<T> :: depth(Node<T>* p) {
	int left = 0;
	int right = 0;
	if (p == NULL)
		return 0;
	else
	{
		 
		left= depth(p->left);
		right = depth(p->right);

		if (left > right)
		{
			return(left + 1);
		}
		else
		{
			return(right + 1);
		}
	}
}

template<class T>
void Tree<T>::balance(vector<T>number, int first, int last) {
	if (first <= last) {
		int middle = (first + last) / 2;
		insert(number[middle]);
		balance(number, first, middle - 1);
		balance(number, middle + 1, last);
	}
}

#endif // Binary_Search_Tree